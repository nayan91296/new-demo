
<?php /*@if ($message = \Session::get('success'))
<div class="pad margin alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = \Session::get('danger'))
<div class="pad margin alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = \Session::get('warning'))
<div class="pad margin alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = \Session::get('info'))
<div class="pad margin alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())
<div class="pad margin alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors.
</div>
@endif */ ?>

<script type="text/javascript">

  @if(Session::has('success'))
  		toastr.success("{{ Session::get('success') }}",'Success');
  @endif


  @if(Session::has('info'))
  		toastr.info("{{ Session::get('info') }}");
  @endif


  @if(Session::has('warning'))
  		toastr.warning("{{ Session::get('warning') }}");
  @endif


  @if(Session::has('danger'))
  		toastr.error("{{ Session::get('danger') }}");
  @endif


</script>