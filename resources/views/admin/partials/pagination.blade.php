<div class="box-footer">
    {{-- {!! $lists->links() !!} --}}
    <div class="row">
        <div class="col-sm-5">
            Showing {{ $lists->firstItem() }} to {{ $lists->lastItem() }} of total {{$lists->total()}} entries
        </div>
        <div class="col-sm-7 text-right">
                {!! $lists->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
</div>