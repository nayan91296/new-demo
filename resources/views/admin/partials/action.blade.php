<div class="btn-group">
    <button type="button" class="btn btn-block btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
    Action
        <i class="fa fa-caret-down"></i>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        @foreach($action_array as $action_type => $action_url)
            @if($action_type == 'edit')
            <li>
                <a href="{{$action_url}}" class="{{ isset($edit_class) ? $edit_class : 'js_edit'}}" title="Edit">Edit</a>
            </li>
            @endif
            @if($action_type == 'view timetable')
            <li>
                <a href="{{$action_url}}" class="dropdown-item js_view" data-record_id="{{$list->id}}"  title="View">View TimeTable</a>
            </li>
            @endif
            @if($action_type == 'edit_slot')
            <li>
                <a href="{{$action_url}}" class="dropdown-item js_modal_add_edit" data-record_id="{{$list->id}}"  data-toggle="modal" data-target="#add_edit_modal" title="Edit">Edit Slot</a>
            </li>
            @endif
            @if($action_type == 'view')
            <li>
                <a href="{{$action_url }}" class="js_view" title="View">View</a>
            </li>
            @endif
            @if($action_type == 'delete')
            {{-- <li class="divider"></li> --}}
            <li>
                <a data-url="{{$action_url}}" data-record_id="{{$list->id}}"  data-toggle="modal" data-target="#modal-delete" title="Delete">Delete</a>
            </li>
            @endif
            @if($action_type == 'status')
            <li>
                @if ($list->status != 1)
                    <a href="{{$action_url}}" class="change_block_status" id="js_status_anchor_{{$list->id}}"  title="In-Active">Active</i></a>
                @else
                    <a href="{{$action_url}}" class="change_block_status" title="Active" id="js_status_anchor_{{$list->id}}">In-Active</a>
                @endif
            </li>
            @endif
             @if($action_type == 'view product_offer')
            {{-- <li class="divider"></li> --}}
            <li>
                <a href="{{$action_url}}" class="js_view" title="Offer">View Product Offer</a>
            </li>
            @endif
            
        @endforeach
    </ul>
</div>