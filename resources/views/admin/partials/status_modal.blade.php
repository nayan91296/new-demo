<div class="modal fade" id="chang_status_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Status Confirmation</h4>
      </div>
      <form action="" method="get" id="chang_status_form">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="row_id" value=""/>
      <div class="modal-body">
        <p>Are you sure want to change this ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="js_status_submit">Save changes</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>