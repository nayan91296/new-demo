<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
  <h4 class="modal-title">{{$data->id ? 'Edit' : 'Add'}} Product</h4>
</div>

    {{ Form::model($data,array('action' => 'Admin\ProductController@anyProductStore','method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data")) }}
        @csrf
        {{Form::hidden('id')}}
        <div class="box-body">
          <div class="form-group col-md-6">
            {{Form::label('name','Name')}}

            {{Form::text('name',null,
                    array(  'placeholder'   => 'Please Enter Product',
                            'class'         => 'form-control',
                            'id'            => 'name',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>
          
          <div class="form-group col-md-6">
            {{Form::label('price','Price')}}

            {{Form::text('price',null,
                    array(  'placeholder'   => 'Please Enter Price',
                            'class'         => 'form-control',
                            'id'            => 'price',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>

            <div class="form-group col-md-6">
                            {{Form::label('icon','Image',array('class'=>'floating-label'))}}<br>
                            
                           
                           {{Form::file('icon',
                           array( 
                                'class'         => 'form-control',
                                'id'            => 'icon',
                                'onchange' => 'image_readURL(this)',
                                )
                               )}}
                           
                           
                            <span class="text-danger js_error_span"></span>

                            <img id="img" src="" alt="" height="160" width="160" style="display: none;border: 1px solid;">
                            @if(isset($data->id))
                                 <img src="{{$data->icon}}" height="160" width="160" id="edit_img">
                            @endif 
          </div>
           <div class="form-group col-md-6">
                {{Form::label('category_id','Category')}}

                {{Form::select('category_id',$category,null,
                    array(
                      'class'=>'form-control',
                      'required' => 'required'
                    )
                )}}
                <span class="help-block js_error_span"></span>
            </div>
             <div class="form-group col-md-6">
                {{Form::label('status','Status')}}

                {{Form::select('status',array(
                      '1' => 'Active',
                      '0' => 'Inactive'
                    ),null,
                    array(
                      'class'=>'form-control',
                      'required' => 'required'
                    )
                )}}
                <span class="help-block js_error_span"></span>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-primary pull-right','id'=>"js_submit_modal"))}}

        </div>
    {{ Form::close() }}

   
