@extends('admin.layouts.master')
@section('title','Product')
@section('page_title','Product')
@section('css')

@endsection
@section('breadcrumb')
  <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Product</li>
  </ol>
@endsection
@section('content')
<div class="row" id="js_main_row">
  @include('admin.product.list',$lists)
</div>
<!-- /.row -->
@include('admin.partials.delete_modal')
@include('admin.partials.status_modal')
@endsection
@section('js')

@endsection
@push('custom_script')
<script type="text/javascript">
</script>

@endpush
