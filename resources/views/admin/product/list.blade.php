@include('common.flash')
<div class="col-xs-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            {{-- <h3 class="box-title">Users</h3> --}}
            <span class="pull-right">
            <a id="js_modal_add_edit" href="{{ action('Admin\ProductController@getCreate') }}" class="btn btn-info js_modal_add_edit" >Add New</a>
          </span>
            <form id="search_form" action="{{ action('Admin\ProductController@getIndex') }}">
                <div class="row">
                    <div class="col-sm-1" >
                        <select name="" id="js_per_page" class="form-control input-sm">
                            @php
                                $per_page_arr = array(10,25,50,100);
                            @endphp
                            @foreach($per_page_arr as $per)
                                <option value="{{$per}}" {{$search['per_page'] == $per ? 'selected' : ''}}>{{$per}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-4" >
                        <div class="input-group input-group-sm hidden-xs " >
                            
                            <input type="text" id="js_search_value" name="search" value="{{isset($search['search']) ? $search['search'] : ''}}" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="button" id="js_search_button" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      <div class="box-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="20px">#</th>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th width="40px">Status</th>
                    <th width="60px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php $n =0; @endphp
                @forelse($lists as $list)
                    <tr class="test">
                        <td id="js_row_id_{{$list->id}}">{{++$n}}</td>
                        <!-- <td>{{$list->photo}}</td> -->
                       
                        <td><a href="{{$list->icon}}" target="_blank"><img src="{{$list->icon}}" width="50px" alt="image"></a></td>
                        <td>{{$list->name}}</td>
                        <td>{{$list->category->name}}</td>
                        <td>{{$list->price}}</td>
                        <td id="js_status_{{$list->id}}">
                            @if($list->status == 1)
                            <span class="label label-success">Active</span>
                            @else
                            <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                        <td>
                            @php
                            $action_array = array(
                            // 'view' => action('Admin\TeacherController@getTeacherView',['id'=>$list->id]),
                            // 'view timetable' => action('Admin\TeacherController@getTimeTable',['id'=>$list->id]),
                            'edit' => action('Admin\ProductController@getEdit',['id'=>$list->id]),
                            'delete' => action("Admin\ProductController@anyDelete"),
                            'status' => action("Admin\ProductController@anyChangeStatus",$list->id),
                            );
                            @endphp
                            @include('admin.partials.action',['action_array'=>$action_array,'list'=>$list,'edit_class'=>'js_modal_add_edit'])
                            
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="15" align="center">
                            Record not found
                        </td>
                    </tr>

                    @endforelse
            </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      @include('admin.partials.pagination',['lists'=>$lists])
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
