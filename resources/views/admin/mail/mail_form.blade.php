

@section('content')

   
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Send Mail</h3>
      </div>

    <!-- Main content -->
   
              <form method="post" action="{{ action('Admin\MailController@anySendMail') }}" enctype="multipart/form-data" id="js_form">
              	@csrf
                <div class="box-body">
                  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name">
                    @if ($errors->has('name'))
                   <span class="text-danger">{{ $errors->first('name') }}</span>
                  @endif
                  </div>
                  <div class="form-group {{ $errors->has('to_mail') ? 'has-error' : ''}}">
                    <label for="to_mail">To mail</label>
                    <input type="text" name="to_mail" class="form-control" id="to_mail" placeholder="Enter to mail">
                    @if ($errors->has('to_mail'))
	          	     <span class="text-danger">{{ $errors->first('to_mail') }}</span>
	                @endif
                  </div>
                  <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                    <label for="subject">Subject</label>
                    <input type="text" name="subject" class="form-control" id="subject" placeholder="subject">
                    @if ($errors->has('subject'))
	          	      <span class="text-danger">{{ $errors->first('subject') }}</span>
	                @endif
                  </div>
                  <div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
                    <label for="message">message</label>
                    <input type="text" name="message" class="form-control" id="message" placeholder="message">
                    @if ($errors->has('message'))
                    <span class="text-danger">{{ $errors->first('message') }}</span>
                  @endif
                  </div>
                  <div class="form-group {{ $errors->has('from_mail') ? 'has-error' : ''}}">
                    <label for="from_mail">from mail</label>
                    <input type="text" name="from_mail" class="form-control" id="from_mail" value="nayanvariya123@gmail.com" disabled="">
                  </div>
                  @if ($errors->has('from_mail'))
	          	    <span class="text-danger">{{ $errors->first('from_mail') }}</span>
	              @endif
                </div>
                <!-- /.card-body -->

                <div class="box-footer">
                  <button type="button" name="submit" class="btn btn-primary" id="js_submit_ajax">Submit</button>
                  <button type="button" name="back" onClick="javascript:history.go(-1);" class="btn btn-secondary"><i
                        class="fa fa-chevron-circle-left"> &nbsp;</i>Back
                     </button>
                </div>
              </form>
            </div>
          </div>
       
     
@endsection