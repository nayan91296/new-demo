<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
  <h4 class="modal-title">{{$data->id ? 'Edit' : 'Add'}} Offer</h4>
</div>

    {{ Form::model($data,array('action' => 'Admin\OfferController@anyOfferStore','method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data")) }}
        @csrf
        {{Form::hidden('id')}}
        <div class="box-body">

          <div class="form-group col-md-6">
            {{Form::label('product','Product')}}

            {{Form::select('product[]',$product,null,
                    array(  'multiple'=>'multiple',
                            'class'         => 'form-control',
                            'id'            => 'product',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>

          <div class="form-group col-md-6">
            {{Form::label('name','Name')}}

            {{Form::text('name',null,
                    array(  'placeholder'   => 'Please Enter Offer Name',
                            'class'         => 'form-control',
                            'id'            => 'name',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>

          <div class="form-group col-md-6">
            {{Form::label('offer','Offer')}}

            {{Form::text('offer',null,
                    array(  'placeholder'   => 'Please Enter Offer',
                            'class'         => 'form-control',
                            'id'            => 'offer',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>
          
            <div class="form-group col-md-6">
            {{Form::label('start_date','Start Date')}}

            {{Form::date('start_date',null,
                    array(  
                            'class'         => 'form-control',
                            'id'            => 'start_date',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>

            <div class="form-group col-md-6">
            {{Form::label('end_date','End Date')}}

            {{Form::date('end_date',null,
                    array(  
                            'class'         => 'form-control',
                            'id'            => 'end_date',
                            'required' => 'required')
                            )}}

            <span class="text-danger js_error_span"></span>
          </div>

           <div class="form-group col-md-6">
                {{Form::label('status','Status')}}

                {{Form::select('status',array(
                      '1' => 'Active',
                      '0' => 'Inactive'
                    ),null,
                    array(
                      'class'=>'form-control',
                      'required' => 'required'
                    )
                )}}
                <span class="help-block js_error_span"></span>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-primary pull-right','id'=>"js_submit_modal"))}}

        </div>
    {{ Form::close() }}
   
