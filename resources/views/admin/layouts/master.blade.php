<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
      @yield('title')
  </title>
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('admin/dist/img/favicon/favicon.png') }}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content={{csrf_token()}}>
  <link rel="stylesheet" href="{{ asset('css/all_in_one.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/toastr.css')}}">
  @yield('css')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div id="preloader">
    {{-- <img id="loading-image" src="{{ asset('loader.gif') }}" draggable="false" alt="loading..."> --}}
  </div>
<div class="wrapper" id="app">
  @include('admin.layouts.header')

  @include('admin.layouts.sidebar')

  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        @yield('page_title')
        <small>@yield('sub_title')</small>
      </h1>
      @yield('breadcrumb')
      {{-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
      </ol> --}}
    </section>
    <section class="content" id="section_content">
        @yield('content')
    </section>
  </div>
  @include('admin.partials.add_edit_modal')
  @include('admin.layouts.footer')
  <!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
  {{-- @include('admin.layouts.control_sidebar') --}}
  <div class="control-sidebar-bg"></div>
</div>
<script type="text/javascript" src="{{asset('js/all_in_one.js')}}"></script>
<script type="text/javascript" src="{{asset('js/toastr.js')}}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<!-- <script type="text/javascript" src="{{asset('js/custom.js')}}"></script> -->
@yield('js')
@stack('custom_script')

</body>
</html>