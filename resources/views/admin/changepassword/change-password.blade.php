@extends('admin.layouts.master')
@section('page_title','Admin Change Password')
@section('title','Change Password')

@section('content')

    <div class="row" id="js_main_row">
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Change Password</h3>
      </div>

    <!-- Main content -->
   
              <form method="post" action="{{ route('change.password') }}" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">
                  <div class="form-group {{ $errors->has('old_pass') ? 'has-error' : ''}}">
                    <label for="old_pass">Old Password</label>
                    <input type="password" name="old_pass" class="form-control" id="old_pass" placeholder="Enter Old Password">
                    @if ($errors->has('old_pass'))
	          	     <span class="text-danger">{{ $errors->first('old_pass') }}</span>
	                @endif
                  </div>
                  <div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
                    <label for="new_password">New Password</label>
                    <input type="password" name="new_password" class="form-control" id="new_password" placeholder="New Password">
                    @if ($errors->has('new_password'))
	          	      <span class="text-danger">{{ $errors->first('new_password') }}</span>
	                @endif
                  </div>
                  <div class="form-group {{ $errors->has('new_password_confirmation') ? 'has-error' : ''}}">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" name="new_password_confirmation" class="form-control" id="confirm_password" placeholder="Confirm Password">
                  </div>
                  @if ($errors->has('new_password_confirmation'))
	          	    <span class="text-danger">{{ $errors->first('new_password_confirmation') }}</span>
	              @endif
                </div>
                <!-- /.card-body -->

                <div class="box-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                  <button type="button" name="back" onClick="javascript:history.go(-1);" class="btn btn-secondary"><i
                        class="fa fa-chevron-circle-left"> &nbsp;</i>Back
                     </button>
                </div>
              </form>
            </div>
          </div>
        </div>   
     
@endsection
@push('custom_script')
@endpush