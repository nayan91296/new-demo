@include('common.flash')
<div class="col-xs-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            {{-- <h3 class="box-title">Users</h3> --}}
            <span class="pull-right">
            <a id="js_back" href="{{ action('Admin\OfferController@getIndex') }}" class="btn btn-info" >Back</a>
          </span>
            <form id="search_form" action="{{ action('Admin\OfferController@getIndex') }}">
                <div class="row">
                    <div class="col-sm-1" >
                        <select name="" id="js_per_page" class="form-control input-sm">
                            @php
                                $per_page_arr = array(10,25,50,100);
                            @endphp
                            @foreach($per_page_arr as $per)
                                <option value="{{$per}}" {{$search['per_page'] == $per ? 'selected' : ''}}>{{$per}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-4" >
                        <div class="input-group input-group-sm hidden-xs " >
                            
                            <input type="text" id="js_search_value" name="search" value="{{isset($search['search']) ? $search['search'] : ''}}" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="button" id="js_search_button" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      <div class="box-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="20px">#</th>
                    <th>Product</th>
                    <th>@sortablelink('name', 'Name',['filter' => 'active, visible'],['class' => 'js_sorting'])</th>
                    <th>Offer</th>
                    <th width="40px">Status</th>
                    <th width="60px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php $n =0; @endphp
                @forelse($lists as $list)
                    <tr class="test">
                        <td id="js_row_id_{{$list->id}}">{{++$n}}</td>
                        <!-- <td>{{$list->photo}}</td> -->
                       
                        {{-- <td><a href="{{$list->icon}}" target="_blank"><img src="{{$list->icon}}" width="50px" alt="image"></a></td> --}}
                        <td>{{$list->product->name}}</td>
                        <td>{{$list->offer->name}}</td>
                        <td>{{$list->offer->offer}} %</td>
                       
                        <td id="js_status_{{$list->id}}">
                            @if($list->status == 1)
                            <span class="label label-success">Active</span>
                            @else
                            <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                        <td>
                            @php
                            $action_array = array(
                            // 'view' => action('Admin\TeacherController@getTeacherView',['id'=>$list->id]),
                            // 'view timetable' => action('Admin\TeacherController@getTimeTable',['id'=>$list->id]),
                            // 'edit' => action('Admin\OfferController@getEdit',['id'=>$list->id]),
                            'delete' => action("Admin\OfferController@anyProductOfferDelete"),
                            'status' => action("Admin\OfferController@anyProductOfferChangeStatus",$list->id),
                            );
                            @endphp
                            @include('admin.partials.action',['action_array'=>$action_array,'list'=>$list,'edit_class'=>'js_modal_add_edit'])
                            
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="15" align="center">
                            Record not found
                        </td>
                    </tr>

                    @endforelse
            </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      @include('admin.partials.pagination',['lists'=>$lists])
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
