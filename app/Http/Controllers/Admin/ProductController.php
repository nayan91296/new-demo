<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    //

    public function getIndex(Request $request)
    { 
        //$lists = Teacher::orderBy('id','desc')->get();

        $lists = new Product();
        $per_page = 10;
        $search['per_page'] = $per_page;

        $lists = $lists->with('category');
        
        if (isset($request->per_page) && !empty($request->per_page)) {
            $per_page = $request->per_page;
            $search['per_page'] = $request->per_page;
        }
        if (isset($request->search) && !empty($request->search)) {
            $search_value = $request->search;
            $search['search'] = $search_value;
           
            $lists = $lists->where('name','like', '%'.$search_value.'%');

             $lists = $lists->orwhereHas('category',function($q) use ($search_value){
                $q->where('categories.name','like', '%'.$search_value.'%');
                
            });

        }

        $lists = $lists->sortable()->paginate($per_page);
        
        if($request->ajax()){
            return view('admin.product.list',compact('lists','search'));
        }

        return view('admin.product.index',compact('lists','search'));
    }

    public function getCreate()
    { 
        $data       = new Product();
        $category = Category::pluck('name', 'id');

        return view('admin.product.add_edit',compact('data','category'));
    }

    public function anyProductStore(Request $request)
    { 

        $response = array('status'=>false, 'redirect_url'=>null, 'errors'=>array());
         if (!empty($request->id)) {

             $rules  = [
                'name'   => 'required',
                // 'icon'    => 'required',
                'price' => 'required'
                
            ];
         }
         else
         {
             $rules  = [
                'name'   => 'required',
                'icon'    => 'required',
                'price' => 'required'
                
            ];
         }
           
          $response['errors'] = $request->validate($rules);
            $data = new Product();
            if (!empty($request->id)) {
            
                $data = Product::find($request->id);
              }
            if ($request->hasFile('icon')){
                $data->icon = $request->icon->store('ProductPhoto','public');
            }
            $status = (isset($request->status)) ? 1 : 0 ; 
            $data->name = $request->name;
            $data->category_id = $request->category_id;
            $data->price = $request->price;
            $data->status = $status;
            
            $data->save();
            
            $response['status'] = true;
            $response['redirect_url'] = action('Admin\ProductController@getIndex');
            Session::flash('success','Record inserted successfully');
            return response()->json($response);  
    }

    public function getEdit($id)
    { 
        $data   = Product::findOrFail($id);
       $category = Category::pluck('name', 'id');
        return view('admin.product.add_edit',compact('data','category'));
    }

    public function anyDelete(Request $request)
    {   
        $id = $request->id;
        Product::where('id',$id)->delete();
        return response()->json(true);
    }

    public function anyChangeStatus($id = null)
    { 
        $response =array('status'=>null,'row_id'=>null);
        $data = Product::findOrFail($id);
        $data->status = ($data->status == 0?1:0);
        $data->save();
        $response =array('status'=>$data->status,'row_id'=>$data->id);
        return response()->json($response);
    }
}
