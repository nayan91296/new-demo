<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class SellerController extends Controller
{
    //

     public function getIndex(Request $request)
    { 
        //$lists = Teacher::orderBy('id','desc')->get();

        $lists = new Admin();
        $per_page = 10;
        $search['per_page'] = $per_page;
        
        if (isset($request->per_page) && !empty($request->per_page)) {
            $per_page = $request->per_page;
            $search['per_page'] = $request->per_page;
        }
        if (isset($request->search) && !empty($request->search)) {
            $search_value = $request->search;
            $search['search'] = $search_value;
           
            $lists = $lists->where('name','like', '%'.$search_value.'%');
        }

        $lists = $lists->where('type','Seller')->sortable()->paginate($per_page);
        
        if($request->ajax()){
            return view('admin.seller.list',compact('lists','search'));
        }

        return view('admin.seller.index',compact('lists','search'));
    }

    public function anySellerDelete(Request $request)
    {  
    	$id = $request->id;
        Admin::where('id',$id)->where('type','Seller')->delete();
        return response()->json(true);
    }

    public function anySellerChangeStatus($id = null)
    { 
        $response =array('status'=>null,'row_id'=>null);
        $data = Admin::findOrFail($id);
        $data->status = ($data->status == 0?1:0);
        $data->save();
        $response =array('status'=>$data->status,'row_id'=>$data->id);
        return response()->json($response);
    }

}
