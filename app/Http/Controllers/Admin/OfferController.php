<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Product;
use App\Models\ProductOffer;
use Illuminate\Support\Facades\Session;

class OfferController extends Controller
{
    //
    public function getIndex(Request $request)
    {
    	$lists = new Offer();
        $per_page = 10;
        $search['per_page'] = $per_page;
        
        if (isset($request->per_page) && !empty($request->per_page)) {
            $per_page = $request->per_page;
            $search['per_page'] = $request->per_page;
        }
        if (isset($request->search) && !empty($request->search)) {
            $search_value = $request->search;
            $search['search'] = $search_value;
           
            $lists = $lists->where('name','like', '%'.$search_value.'%');
        }

        $lists = $lists->sortable()->paginate($per_page);
        
        if($request->ajax()){
            return view('admin.offer.list',compact('lists','search'));
        }

        return view('admin.offer.index',compact('lists','search'));
    	
    }

     public function getProductOffer(Request $request, $id=null)
    {  
    	$lists = new ProductOffer();
        $per_page = 10;
        $search['per_page'] = $per_page;

        $lists = $lists->with(['offer','product']);
        
        if (isset($request->per_page) && !empty($request->per_page)) {
            $per_page = $request->per_page;
            $search['per_page'] = $request->per_page;
        }
        if (isset($request->search) && !empty($request->search)) {
            $search_value = $request->search;
            $search['search'] = $search_value;
           
            $lists = $lists->where('name','like', '%'.$search_value.'%');
        }

        $lists = $lists->where('offer_id',$id)->sortable()->paginate($per_page);
        
        if($request->ajax()){


            return view('admin.product_offer.list',compact('lists','search'));
        }

        return view('admin.product_offer.index',compact('lists','search'));
    	
    }

    public function getCreate()
    { 
        $data       = new Offer();
        $product = Product::pluck('name', 'id');
        return view('admin.offer.add_edit',compact('data','product'));
    }

    public function anyOfferStore(Request $request)
    { 

        $response = array('status'=>false, 'redirect_url'=>null, 'errors'=>array());
        if(!empty($request->id))
        {
            $rules  = [
            	// 'product' => 'required',
                'name'   => 'required',
                'offer'    => 'required',
                'start_date'    => 'required',
                'end_date'    => 'required'
                
            ];
        }
        else
        {
        	$rules  = [
            	'product' => 'required',
                'name'   => 'required',
                'offer'    => 'required',
                'start_date'    => 'required',
                'end_date'    => 'required'
                
            ];
        }
            
           $response['errors'] = $request->validate($rules);

           if(!empty($request->id))
           {
                $data = Offer::find($request->id);

            	$status = (isset($request->status)) ? 1 : 0 ; 
                $data->name = $request->name;
                $data->offer = $request->offer;
                $data->start_date = $request->start_date;
                $data->end_date = $request->end_date;
                $data->status = $status;
           
                $data->save();
           }
           else
           {
           	$data = new Offer();

            	$status = (isset($request->status)) ? 1 : 0 ; 
                $data->name = $request->name;
                $data->offer = $request->offer;
                $data->start_date = $request->start_date;
                $data->end_date = $request->end_date;
                $data->status = $status;
           
                $data->save();

            for($i=0 ; $i<sizeof($request->product) ; $i++)
             {
            	$pro = new ProductOffer();
            	$pro->product_id = $request->product[$i];
            	$pro->status = 1; 
            	$pro->offer_id = $data->id;
                
            	$pro->save();
             } 
           }
             
            $response['status'] = true;
            $response['redirect_url'] = action('Admin\OfferController@getIndex');
            Session::flash('success','Record inserted successfully');
            return response()->json($response);  
    }

    public function getEdit($id)
    { 
        $data   = Offer::findOrFail($id);
       $product = Product::pluck('name', 'id');
        return view('admin.offer.add_edit',compact('data','product'));
    }

    public function anyDelete(Request $request)
    {   
        $id = $request->id;
        Offer::where('id',$id)->delete();
        return response()->json(true);
    }

    public function anyProductOfferDelete(Request $request)
    {
    	$id = $request->id;
        ProductOffer::where('id',$id)->delete();
        return response()->json(true);
    }

    public function anyChangeStatus($id = null)
    { 
        $response =array('status'=>null,'row_id'=>null);
        $data = Offer::findOrFail($id);
        $data->status = ($data->status == 0?1:0);
        $data->save();
        $response =array('status'=>$data->status,'row_id'=>$data->id);
        return response()->json($response);
    }

    public function anyProductOfferChangeStatus($id = null)
    {
    	$response =array('status'=>null,'row_id'=>null);
        $data = ProductOffer::findOrFail($id);
        $data->status = ($data->status == 0?1:0);
        $data->save();
        $response =array('status'=>$data->status,'row_id'=>$data->id);
        return response()->json($response);
    }
}
