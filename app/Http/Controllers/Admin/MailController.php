<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Illuminate\Support\Facades\Session;

class MailController extends Controller
{
    //
    public function getIndex(Request $request)
    {
        return view('admin.mail.index');
    }

    public function anySendMail(Request $request)
    {
    	$response = array('status'=>false, 'redirect_url'=>null, 'errors'=>array());

        $rules = [
            'to_mail' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
            'name' => 'required'

        ];
        $response['errors'] = $request->validate($rules);

    	$to_mail = $request->to_mail;
    	$message = $request->message;
    	$subject = $request->subject;
    	
        $info = array('name'=>$request->name,'body'=>$message);
        $res = Mail::send('admin.mail.mail_msg', $info, function($message) use ($to_mail) {
        $message->to($to_mail)->subject('test');
        $message->from('nayanvariya123@gmail.com');
        });

        Session::flash('success','Mail Sent successfully');
        return back()->with('success','Mail Sent Successfully');
    } 
}
