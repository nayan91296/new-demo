<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    //
    public function getIndex(Request $request)
    { 
        //$lists = Teacher::orderBy('id','desc')->get();

        $lists = new Category();
        $per_page = 10;
        $search['per_page'] = $per_page;
        
        if (isset($request->per_page) && !empty($request->per_page)) {
            $per_page = $request->per_page;
            $search['per_page'] = $request->per_page;
        }
        if (isset($request->search) && !empty($request->search)) {
            $search_value = $request->search;
            $search['search'] = $search_value;
           
            $lists = $lists->where('name','like', '%'.$search_value.'%');
        }

        $lists = $lists->sortable()->paginate($per_page);
        
        if($request->ajax()){
            return view('admin.category.list',compact('lists','search'));
        }

        return view('admin.category.index',compact('lists','search'));
    }

    public function getCreate()
    { 
        $data       = new Category();
        return view('admin.category.add_edit',compact('data'));
    }

    public function anyCategoryStore(Request $request)
    { 

        $response = array('status'=>false, 'redirect_url'=>null, 'errors'=>array());
        if (!empty($request->id)) {
            $rules  = [
                'name'   => 'required|unique:categories,name,'.$request->id,
               
            ];
        }
        else
        {
            $rules  = [
                'name'   => 'required|unique:categories,name,'.$request->id,
                'icon'    => 'required',
                
            ];
        }
            
          $response['errors'] = $request->validate($rules);
            $data = new Category();
            if (!empty($request->id)) {
            
                $data = Category::find($request->id);
              }
            if ($request->hasFile('icon')){
                $data->icon = $request->icon->store('CategoryPhoto','public');
            }
            $status = (isset($request->status)) ? 1 : 0 ; 
            $data->name = $request->name;
            $data->status = $status;
            
            $data->save();
            
            $response['status'] = true;
            $response['redirect_url'] = action('Admin\CategoryController@getIndex');
            Session::flash('success','Record inserted successfully');
            return response()->json($response);  
    }

    public function getEdit($id)
    { 
        $data   = Category::findOrFail($id);
       
        return view('admin.category.add_edit',compact('data'));
    }

    public function anyDelete(Request $request)
    {   
        $id = $request->id;
        Category::where('id',$id)->delete();
        return response()->json(true);
    }

    public function anyChangeStatus($id = null)
    { 
        $response =array('status'=>null,'row_id'=>null);
        $data = Category::findOrFail($id);
        $data->status = ($data->status == 0?1:0);
        $data->save();
        $response =array('status'=>$data->status,'row_id'=>$data->id);
        return response()->json($response);
    }
}
