<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class DashboardController extends Controller
{
    //
    public function index()
    {
    	$total_category = Category::count();
    	$total_product = Product::count();
    	return view('admin.dashboard',compact('total_category','total_product'));
    }
}
