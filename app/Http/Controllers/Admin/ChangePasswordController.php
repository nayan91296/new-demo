<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    //
    public function changePassword()
	{
		return view("admin.changepassword.change-password");
	}

    public function changePasswordCreate(Request $request)
	{

		$this->validate($request, [
			'old_pass' => 'required',
			'new_password' => 'required|confirmed|min:6',
		]);
        
		$id = Auth::id();
		
		$select = Admin::where('id',$id)->first();
		//dd($select);
		$password = $select->password;

		if(Hash::check($request->old_pass,$password))
		{
			//dd('same');
			$newPass = bcrypt($request->new_password);
			//dd($newPass);
            Admin::where('id',$id)->update(['password'=>$newPass]);
            return redirect()->route('change.password')->with('success', 'Password Update Successfully...');
		}
		else
		{
			return redirect()->route('change.password')->with('danger', 'Old Password does not match with our database');
		}
	}
}
