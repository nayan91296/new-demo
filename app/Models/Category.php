<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    //
    use Sortable;

    public function getIconAttribute($value)
    {
        return asset('storage').'/'.$value;
    }

    
}
