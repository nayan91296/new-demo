<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ProductOffer extends Model
{
    //
    use Sortable;

    public function product()
    {
    	 return $this->belongsTo(Product::class);
    }

    public function offer()
    {
    	 return $this->belongsTo(Offer::class);
    }
}
