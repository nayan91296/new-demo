<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    //
     use Sortable;

     public function getIconAttribute($value)
      {
        return asset('storage').'/'.$value;
      }

    public function category()
    {
    	 return $this->belongsTo(Category::class,'category_id','id');
    }
}
