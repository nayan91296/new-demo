<?php 

Route::get('/',function(){
    return redirect()->route('admin.login');
});

Route::group(['namespace'=>'Admin'],function() {

        Route::get('/seller/register', 'RegisterController@showRegistrationForm')->name('admin.register');
        Route::post('/seller/register', 'RegisterController@create')->name('admin.registered');
        Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('login', 'LoginController@login');

Route::group(['middleware' => 'auth:admin'], function () {

        Route::post('logout', 'LoginController@logout')->name('admin.logout');
        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::get('change-password', 'ChangePasswordController@changePassword')->name('change.password');
        Route::post('change-password', 'ChangePasswordController@changePasswordCreate')->name('change.password');

   });
});

Route::group(['middleware'=>'auth:admin'],function() {
   

    AdvancedRoute::controller('/category', 'Admin\CategoryController');
    AdvancedRoute::controller('/product', 'Admin\ProductController');
    AdvancedRoute::controller('/mail', 'Admin\MailController');
    AdvancedRoute::controller('/offer', 'Admin\OfferController');
    AdvancedRoute::controller('/seller', 'Admin\SellerController');


});