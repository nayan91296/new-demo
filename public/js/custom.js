$(document).ready(function(){
    myTable = $('#my_table').DataTable();

    function scroll_to_error(){
        $('html, body').animate({
            scrollTop: $('.has-error:first').offset().top-80
        }, 1300);
    }

    function scroll_top(){
        $('html, body').animate({
            scrollTop: $('body').offset().top-80
        }, 1100);
    }

    function fade_effect(time = null){
       $('#js_main_row').fadeToggle(time);
    }

	// Delete single recode.
	$('body #modal-delete').on('show.bs.modal',function(event){
        var button = $(event.relatedTarget)
        var recod_id = button.data('record_id');
        var url = button.data('url');
        var modal = $(this)
        modal.find('.modal-content #record_id').val(recod_id)
        modal.find('.modal-content #single_delete_form').attr('action', url)
    });

    $('body #js_main_row').on('click','.change_block_status',function(event){
        event.preventDefault();
        url = $(this).attr('href');
        $("#chang_status_modal").modal("show");
        var delete_modal = $("#chang_status_modal");
        delete_modal.find('.modal-content #chang_status_form').attr('action', url)
    });

    $("#js_submit").click(function(){
        $('.text-danger').html('');
        my_form = $("#js_form");
        var form_data = new FormData(my_form[0]);
        var form_url = $("#js_form").attr('action');       
        $.ajax({
            type:"Post",
            url:form_url,
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(result){
                if (result.status) {
                    if (result.redirect_url != '') {
                        window.location.href = result.redirect_url;
                    }
                }else{
                    $.each(result.errors, function( k, v ){
                        my_form.find('#'+k).next('span').html(v);
                    });
                }
            }
        })        
    })

    $("body").on('click','#js_delete_record',function(e){
        e.preventDefault();
        $('.text-danger').html('');
        var form_data = $('#modal-delete').find("#single_delete_form").serialize();
        var record_id = $('#modal-delete').find("#record_id").val();
        var form_url = $('#modal-delete').find("#single_delete_form").attr('action');
        $.ajax({
            type:"Post",
            url:form_url,
            data:form_data,
            // dataType:'html',
            success:function(result){
                // $("#js_main_row").find("#js_row_id_"+record_id).parent('tr').remove();
                if (result.redirect_url != '') {
                    redirect_url(result.redirect_url);
                }
                $('#modal-delete').modal('hide');
                toastr.error("Record successfully deleted!",'Delete');
            }
        })        
    })

    $("body").on('click','#js_status_submit',function(e){
        e.preventDefault();
        var form_url = $('#chang_status_modal').find("#chang_status_form").attr('action');
        $.ajax({
            type:"get",
            url:form_url,
            success:function(result){
                if (result.status ==0) {
                    $("#js_status_anchor_"+result.row_id).text('Active');
                    $("#js_status_anchor_"+result.row_id).attr('title','Active');
                    $("#js_status_"+result.row_id).html('<span class="label label-danger">Inactive</span>');
                }else{
                    $("#js_status_anchor_"+result.row_id).text('In-Active');
                    $("#js_status_anchor_"+result.row_id).attr('title','In-Active');
                    $("#js_status_"+result.row_id).html('<span class="label label-success">Active</span>');
                }
                $('#chang_status_modal').modal('hide');
                toastr.success("Record status successfully changed!",'Success');
            }
        })        
    })

    $("body").on('click',".js_modal_add_edit",function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                $('#add_edit_modal').find('.modal-content').html(response);
                $('#add_edit_modal').modal("show");
            }
         });
    })

    $("body").on('click',"#js_submit_modal",function(){
        $('.js_error_span').html('');
        my_form = $("#add_edit_modal").find("#js_modal_form");
        my_form.find('.has-error').removeClass("has-error");
        var form_data = new FormData(my_form[0]);
        var form_url = $("#add_edit_modal").find("#js_modal_form").attr('action');       
        $.ajax({
            type:"Post",
            url:form_url,
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(result){
                if (result.status) {
                    $('#add_edit_modal').modal("hide");
                    if (result.redirect_url != '') {
                        redirect_url(result.redirect_url);
                    }
                }
            },
            error:function(data){
                var result = data.responseJSON;
                $.each(result.errors, function( k, v ){
                    my_form.find('#'+k).parent('div .form-group').addClass('has-error');
                    my_form.find('#'+k).parent('div .form-group').find('.js_error_span').html(v);
                });
            }
        })        
    })

    function redirect_url(url=null){
        fade_effect();
        $.get(url, {}, function(data){
            $('#js_main_row').html(data);
            fade_effect('slow');
            scroll_top();
        });
    }

    // On edit it will redirect on given url
    $("body").on('click',".js_edit",function(e){
        e.preventDefault();
        url = $(this).attr('href');
        redirect_url(url);
    });

    // On view it will redirect on given url
    $("body").on('click',".js_view",function(e){
        e.preventDefault();
        url = $(this).attr('href');
        redirect_url(url);
    });

    // On Back it will redirect on given url
    $("body").on('click','#js_back',function(e){
        e.preventDefault();
        url = $(this).attr('href');
        redirect_url(url);
    });

    // On Add it will redirect on given url
    $("body").on('click','#js_add_new',function(e){
        e.preventDefault();
        url = $(this).attr('href');
        redirect_url(url);
    });

    $("body").on('click',"#js_submit_ajax",function(){
        $('.js_error_span').html('');
        my_form = $("#js_main_row").find("#js_form");
        my_form.find('.has-error').removeClass("has-error");
        var form_data = new FormData(my_form[0]);
        var form_url = $("#js_form").attr('action');    
        $.ajax({
            type:"Post",
            url:form_url,
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(result){  
               
               if(result.status) {
                   if(result.redirect_url != '')
                   {
                    redirect_url(result.redirect_url);  
                   }
                    
                }

                else{
                    $.each(result.errors, function( k, v ){
                        my_form.find('#'+k).parent('div .form-group').addClass('has-error');
                        my_form.find('#'+k).parent('div .form-group').find('.js_error_span').html(v);
                    });
                    scroll_to_error();
                }
            }
        })        
    })
    
    $('#js_main_row').on('click','.page-item > a.page-link',function(e){
        e.preventDefault();
        url = $(this).attr('href');
        per_page = $('#js_per_page').val();
        search = $('#js_search_value').val();
        $.get(url,{'per_page':per_page, 'search':search}, function(data){
            $("#js_main_row").html(data);
        });
    });

    $('#js_main_row').on('click','#js_search_button',function(e){
        e.preventDefault();
        url = $('#search_form').attr('action');
        per_page = $('#js_per_page').val();
        search = $('#js_search_value').val();
        $.get(url,{'per_page':per_page, 'search':search}, function(data){
            $("#js_main_row").html(data);
        });
    });

    $('#js_main_row').on('change','#js_per_page',function(e){
        e.preventDefault();
        $("#js_search_button").trigger("click");
    });

    $('#js_main_row').on('click','.js_sorting',function(e){
        e.preventDefault();
        url  = $(this).attr('href');
        per_page = $('#js_per_page').val();
        search = $('#js_search_value').val();
        $.get(url,{'per_page':per_page, 'search':search}, function(data){
            $("#js_main_row").html(data);
        });
    });
})


$(document).ajaxStart(function() {
    Pace.restart();
    $("#preloader").show();
});

$(document).ajaxStop(function() {
    $( "#preloader" ).fadeOut( "slow", function() {
        $("#preloader").hide();
    });
});

// $(document).ajaxSend(function(event, jqxhr, settings){
//     $("#section_content").LoadingOverlay("show");
// });
// $(document).ajaxComplete(function(event, jqxhr, settings){
//     $("#section_content").LoadingOverlay("hide"); 
// });

$(window).on('beforeunload', function (e) {
    $("#preloader").show();
});

$( window ).on('load', function() {
    $( "#preloader" ).fadeOut( "slow", function() {
        $("#preloader").hide();
        // Animation complete.
    });
});
